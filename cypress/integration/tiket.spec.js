/// <reference types="cypress" />

describe('Tikets', () => {

    beforeEach(() => cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html'))

    it('Deve preencher todos os campos de texto', () => {
        const firstName = "Fagundes";
        const lastName = "Fernandes";
        const fullName = `${firstName} ${lastName}`

        cy.get('#first-name').type(firstName);
        cy.get('#last-name').type(lastName);
        cy.get('#email').type('fagundesfernandes94@gmail.com');
        cy.get('#requests').type('Amante da Marvel');
        // interagindo com seletores
        cy.get('#ticket-quantity').select('2');
        // interagindo com ragio buttons
        cy.get('#vip').check();
        // integragindo com checkboxes
        cy.get('#friend').check();
        cy.get('#publication').check();


        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        )

        cy.get('#agree').click();
        cy.get('#signature').type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get(".reset").click()

        cy.get("@submitButton").should("be.disabled")


    });



    it('Deve conter o nome TIKETBOX no cabeçalho', () => {
        cy.get("header h1").should("contain", "TICKETBOX");

    });

    it('Quando o email é informado inválido', () => {
        cy.get('#email')
            .as("email")
            .type('fagundesfernandes94gmail.com');

        cy.get("@email").should("exist")

    });

    it('Preencher os campos obrigatórios', () => {
        const customer = {
            firstName: "Alfredo",
            lastName: "Silva",
            email: "alfredo@gmail.com"
        };

        cy.fillMandatoryFilds(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("#agree").uncheck()

        cy.get("@submitButton").should("be.disabled")

    });

})